try(DestroyDialog holoMaker) Catch()


-- Subject = undefined 
-- ViewWidth = 500
-- ViewHeight = 500
/*
b = #(1,2,3)
c = #(4,5,6)
d = #(7,8,9)

a = #(b,c,d)

fila = a.count
columna = a[1].count 

coord = #()
--print columna
*/



Function Rot180Array Arr = 
(
	columna = Arr[1].count
	fila = Arr.count
	ModArr = #()
	for n = 1 to Arr.count do
	(
		ModArr[n]= #()
	)
	
	--print coord
	
	for x = 1 to Arr.count do 
	(
		
		for y = 1 to Arr[1].count  do
		(
			ModArr[x][y] = Arr[columna][fila]
			
			fila -= 1	
		)
		
		fila= Arr.count
		columna -= 1

	)
		
	ModArr
	
)

Function RotCWArray Arr = 
(
	fila = Arr.count
	ModArr = #()
	for n = 1 to Arr.count do
	(
		ModArr[n]= #()
	)
	
	--print coord
	for x = 1 to Arr.count do 
	(
		
		for y = 1 to Arr[1].count  do
		(
			ModArr[y][x] = Arr[fila][y]
			
		)
		fila -= 1
	)
	
	ModArr
	
)

Function RotCCWArray Arr = 
(
	columna = Arr[1].count
	ModArr = #()
	for n = 1 to Arr.count do
	(
		ModArr[n]= #()
	)
	
	--print coord
	for x = 1 to Arr.count do 
	(
		
		for y = 1 to Arr[1].count  do
		(
			ModArr[x][y] = Arr[y][columna]
			
		)
		columna -= 1
	)
	
	ModArr
	
)

Function FlipArray Arr  = 
(
	columna = Arr[1].count
	ModArr = #()
	for n = 1 to Arr.count do
	(
		ModArr[n]= #()
	)
	
	--print coord
	for x = 1 to Arr.count do 
	(
		
		for y = 1 to Arr[1].count  do
		(
			ModArr[x][y] = Arr[columna][y]
			
		)
		columna -= 1
	)
	
	ModArr
	
)


Function CameraRender ProgBar CamBitmap CameraToRender CamWidth CamHeight Flip RotCW RotCCW Rot180 =
(
	ProgBar.value = 10
	CamRender = render camera: CameraToRender outputwidth: CamWidth outputheight: CamHeight vfb: false
	
	CameraRenderD = #()
	
	for height in 1 to CamHeight do
	(
		CameraRenderD[height] = getpixels CamRender [0,height-1] CamWidth linear:true
	)	

	If (Flip == true) do
	(
		CameraRenderD = FlipArray CameraRenderD
	)

	If (RotCW== true) do
	(
		CameraRenderD = RotCWArray  CameraRenderD
	)

	If (RotCCW == true) do
	(
		CameraRenderD = RotCCWArray 	 CameraRenderD
	)

	If (Rot180== true) do
	(
		CameraRenderD = Rot180Array  CameraRenderD
	)

	NewBitmap = bitmap CamWidth CamHeight color:White

	for height =1 to CamHeight do
	(
		setpixels NewBitmap [0, height-1] CameraRenderD [height]
	)

	ViewPreview = bitmap 136 136 color: Black
	copy NewBitmap ViewPreview
	CamBitmap.bitmap = ViewPreview
	ProgBar.value = 100
	NewBitmap
)



Function CreateHoloCams camName camTargetName camPos camTargetPos subject =
(
	-----------------------------------------------------------------------------------------------------POSITION OF CAMARA---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	newCam = Targetcamera fov:45 nearclip:1 farclip:1000 nearrange:0 farrange:1000 mpassEnabled:off mpassRenderPerPass:off pos:camPos isSelected:on target:(Targetobject transform:(matrix3 [1,0,0] [0,1,0] [0,0,1] camTargetPos))
	newCam.name =  camName
	newCam.target.name = camTargetName
	newCam.target.parent = subject
	newCam.wirecolor = green


)

Function CamaraHeight val =
(
	$HoloCamFront.pos.z = val
	$HoloCamBack.pos.z =  val
	$HoloCamRight.pos.z =  val
	$HoloCamLeft.pos.z   =  val
	
	
)

Function CamaraDistanceChange camTargetPos val =
(
	$HoloCamFront.pos =  (((normalize($HoloCamFront.pos - camTargetPos))*val) + camTargetPos)
	$HoloCamBack.pos =    (((normalize($HoloCamBack.pos - camTargetPos))*val) + camTargetPos)
	$HoloCamRight.pos =  (((normalize($HoloCamRight.pos - camTargetPos))*val) + camTargetPos)
	$HoloCamLeft.pos   =  (((normalize($HoloCamLeft.pos - camTargetPos))*val) + camTargetPos)
	
)

rollout holoMaker "Holo Maker" width:552 height:528
(
	pickbutton 'UI_SelectHero' "Select Object" pos:[8,13] width:150 height:37 align:#left
	button 'UI_CreateCameras' "Create Cameras" pos:[9,57] width:150 height:37 align:#left  enabled:false
	spinner 'UI_CameraDistance' "" pos:[104,109] width:56 height:16 range:[0,100,0] align:#left enabled:false
	spinner 'UI_CameraHeight' "" pos:[104,134] width:56 height:16 range:[0,100,0] align:#left enabled:false
	button 'UI_Render' "Render" pos:[8,184] width:150 height:37 align:#left enabled:false
	bitmap 'UI_Preview' "Bitmap" pos:[168,13] width:376 height:267 align:#left
	spinner 'UI_StartFrame' "" pos:[104,235] width:56 height:16 range:[0,100,0] align:#left
	spinner 'UI_EndFrame' "" pos:[104,260] width:56 height:16 range:[0,100,0] align:#left
	label 'lbl2' "Camera Dist:" pos:[10,109] width:64 height:16 align:#left
	label 'lbl3' "Camera Height:" pos:[10,134] width:86 height:16 align:#left
	label 'lbl4' "End Frame:" pos:[9,260] width:56 height:16 align:#left
	label 'lbl5' "Start Frame:" pos:[9,235] width:64 height:16 align:#left
	dropDownList 'UI_Device' "Device:" pos:[8,296] width:104 height:40 align:#left
	checkbox 'UI_Custom' "Custom Resolution" pos:[120,320] width:115 height:16 align:#left
	spinner 'UI_Height' "" pos:[435,320] width:56 height:16 range:[0,100,0] align:#left
	spinner 'UI_Width' "" pos:[314,321] width:56 height:16 range:[0,100,0] align:#left
	label 'lbl6' "Width:" pos:[264,321] width:38 height:16 align:#left
	label 'lbl7' "Height:" pos:[387,321] width:38 height:16 align:#left
	groupBox 'grp1' "Render Preview" pos:[8,344] width:528 height:168 align:#left
	bitmap 'UI_PreviewFront' "Bitmap" pos:[18,380] width:120 height:120 align:#left
	bitmap 'UI_PreviewLeft' "Bitmap" pos:[275,378] width:120 height:120 align:#left
	bitmap 'UI_PreviewBack' "Bitmap" pos:[146,379] width:120 height:120 align:#left
	bitmap 'UI_PreviewRight' "Bitmap" pos:[405,378] width:120 height:120 align:#left
	progressBar 'pb1' "ProgressBar" pos:[19,359] width:120 height:16 color:(color 30 10 190) align:#left
	progressBar 'pb2' "" pos:[145,359] width:120 height:16 color:(color 30 10 190) align:#left
	progressBar 'pb3' "" pos:[276,359] width:120 height:16 color:(color 30 10 190) align:#left
	progressBar 'pb4' "" pos:[406,358] width:120 height:16 color:(color 30 10 190) align:#left
	progressBar 'pb5' "" pos:[168,288] width:376 height:16 color:(color 30 10 190) align:#left
	
	global g_heroObject = undefined
	global g_camera_front = undefined
	global g_camera_back = undefined
	global g_camera_right = undefined
	global g_camera_left = undefined
	
	on UI_SelectHero picked obj do
	(
		if obj != undefined then
		(
			UI_SelectHero.text = obj.name
			g_heroObject = obj
			
			UI_CreateCameras.enabled = true
			
			
			
			
			
		)
		else
		(
			messagebox "Error! Select Object"
		)
	)
	on UI_CreateCameras pressed do
	(
		camTargetPos = ((g_heroObject.max + g_heroObject.min )/2)
		camDist = ((length(g_heroObject.max-g_heroObject.min))*1.2)
		camHeight = g_heroObject.max.z
		
		camFrontPos  = [g_heroObject.pos.x,				g_heroObject.pos.y - camDist,  camHeight]
		camBackPos   = [g_heroObject.pos.x,				g_heroObject.pos.y + camDist, camHeight]
		camRightPos   = [g_heroObject.pos.x + camDist,g_heroObject.pos.y , 			   camHeight]
		camLeftPos    = [g_heroObject.pos.x - camDist ,g_heroObject.pos.y , 			   camHeight]
		
		Try(delete $HoloCam*)Catch()
		----create Cam             camName                camTargetName            camPos        camTargetPos     subject 
		CreateHoloCams "HoloCamFront" "HoloCamFrontTarget" camFrontPos camTargetPos g_heroObject
		CreateHoloCams "HoloCamBack"  "HoloCamBackTarget" camBackPos camTargetPos   g_heroObject
		CreateHoloCams "HoloCamRight" "HoloCamRightTarget" camRightPos camTargetPos   g_heroObject
		CreateHoloCams "HoloCamLeft"  "HoloCamLeftTarget"    camLeftPos  camTargetPos   g_heroObject
		
			-------------------ENABLE SPINNERS-------------------------------------------------------------------------------
		UI_CameraDistance.enabled = true
		UI_CameraDistance.value = camDist
		
		UI_CameraHeight.enabled = true
		UI_CameraHeight.value = camHeight
		
		UI_Render.enabled = true
	)
	on UI_CameraHeight changed val do
	(
		CamaraHeight val
	)
	
	on UI_CameraDistance changed val do
	(
		CamaraDistanceChange $HoloCamFrontTarget.pos val
	)
	--------------------------------------------------RENDER-----------------------------------------------------------------------------------------------------
	on UI_Render pressed do
(
	--FrontRender = bitmap 500 500 color: Black
	--BackRender = bitmap 500 500 color: Black
	--RightRender = bitmap 500 500 color: Black
	--LeftRender = bitmap 500 500 color: Black
	--					 CameraRender ProgBar CamBitmap CameraToRender CamWidth CamHeight     Flip      RotCW    RotCCW      Rot180
	FrontRender = CameraRender  pb1 UI_PreviewFront $HoloCamFront 500 500   true    false      false       false
	BackRender =  CameraRender  pb2 UI_PreviewBack $HoloCamBack 500 500     true     false     false        true
	RightRender = CameraRender  pb3  UI_PreviewLeft $HoloCamRight 500 500    true 		false 		true      false
	LeftRender =  CameraRender  pb4  UI_PreviewRight  $HoloCamLeft 500 500    true 	true 		false 		false
	
	
	pb5.value = 10
	PreviewFinalRender = bitmap 2048 1536 color: black
	
	FrontImgXCoord = (2048/2) - (500/2)
	FrontImgYCoord = (1536/2) + (500/2)
	pasteBitmap FrontRender PreviewFinalRender (box2 0 0 500 500) [ FrontImgXCoord,FrontImgYCoord]

	BackImgXCoord = (2048/2) - (500/2)
	BackImgYCoord = (1536/2) - (500/2 + 500)
	pasteBitmap BackRender PreviewFinalRender (box2 0 0 500 500) [ BackImgXCoord,BackImgYCoord]	
	
	RightImgXCoord = (2048/2) + (500/2)
	RightImgYCoord = (1536/2) - (500/2)
	pasteBitmap RightRender PreviewFinalRender (box2 0 0 500 500) [ RightImgXCoord,RightImgYCoord]
	
	LeftImgXCoord = (2048/2) - (500/2 + 500)
	LeftImgYCoord = (1536/2) - (500/2)
	pasteBitmap LeftRender PreviewFinalRender (box2 0 0 500 500) [ LeftImgXCoord,LeftImgYCoord]
	
	
	
	
	
	FinalRender = Bitmap  376 267 color:black
	copy  PreviewFinalRender FinalRender
	UI_Preview.bitmap = FinalRender
	pb5.value = 100
	
	FinalRender.filename = @"C:\Users\Administrator\Desktop\HoloImg.jpg"
	save FinalRender
	
)
	
	
	
	
)
createdialog holoMaker

	
	--Rot180 a coord